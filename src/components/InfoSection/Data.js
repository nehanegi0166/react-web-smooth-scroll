export const homeObjOne = {
    id : 'about',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Premium Bank',
    headline: 'Unlimited Transactions with zero fees',
    description: 'Get access to our exclusive app that allows you to send unlimited transactions without getting charged any fees.',
    buttonLabel: 'Get started',
    imgStart: false,
    img: require('../../images/premium-benefits.svg').default,
    alt: 'Car',
    dark:true,
    primary:true,
    darkText:false
};

export const homeObjTwo = {
    id : 'discover',
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Unlimited Access',
    headline: 'Login to your account at any time',
    description: 'We have you covered no matter where you are located. All you need is an internet connection and a phone or computer.',
    buttonLabel: 'Learn More',
    imgStart: false,
    img: require('../../images/undraw-savings.svg').default,
    alt: 'Bank',
    dark:false,
    primary:false,
    darkText:true
};

export const homeObjThree = {
    id : 'signup',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Join our Team',
    headline: 'Creating an account is extremely easy',
    description: 'Get access to our exclusive app that allows you to send unlimited transactions without getting charged any fees.',
    buttonLabel: 'Start Now',
    imgStart: false,
    img: require('../../images/visualization.svg').default,
    alt: 'Paper',
    dark:true,
    primary:true,
    darkText:false
};